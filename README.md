## Introduction

This is a simple implementation for Travello Coding Challenge created by Theo Huang. This application
has 3 APIs to generate a Checkout, scan items and get total price of a Checkout. Please note that the
application initially seeds Item A, B, C, D to the in-memory database.

---

## How to build and execute the application

1. Download the git repository to your machine.
2. Open a terminal and go to the root directory of the project.
3. Run ```./gradlew bootRun``` and the application should start to be running on port 8005
** If port 8005 is not available, you can go to src/main/java/resources/application.properties file and change ```server.port```.

---

## How to run tests

1. Open a terminal and go to the root directory of the project.
2. Run ```./gradlew test``` and the tests should be running

---

## APIs

1. GET /start-checkout: This API generates a new Checkout and returns the ID of the Checkout.

2. GET /scan?checkoutId=<NUMBER>&itemName=<TEXT>: This API scans an item and add the item to the Checkout.

3. GET /total-price?checkoutId=<NUMBER>: This API returns the total price of the Checkout.
