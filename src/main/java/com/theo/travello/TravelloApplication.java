package com.theo.travello;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories("com.theo.travello.db.repository")
@EntityScan("com.theo.travello.db")
public class TravelloApplication {

	public static void main(String[] args) {
		SpringApplication.run(TravelloApplication.class, args);
	}

}
