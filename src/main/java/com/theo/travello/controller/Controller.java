package com.theo.travello.controller;

import com.theo.travello.db.entity.Checkout;
import com.theo.travello.db.entity.Item;
import com.theo.travello.db.repository.CheckoutRepository;
import com.theo.travello.db.repository.ItemRepository;
import com.theo.travello.model.CheckOut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.HashMap;
import java.util.Map;

@org.springframework.stereotype.Controller
public class Controller {

    @Autowired
    private ItemRepository itemRepository;

    @Autowired
    private CheckoutRepository checkoutRepository;

    // Init a new Checkout persistable instance
    @RequestMapping(value = "/start-checkout", method = RequestMethod.GET)
    ResponseEntity<Long> startNewCheckout() {
        return ResponseEntity.ok(checkoutRepository.save(new Checkout()).checkoutId);
    }

    @RequestMapping(value = "/scan", method = RequestMethod.GET)
    ResponseEntity<String> scan(@RequestParam(value = "checkoutId") Long checkoutId, @RequestParam(value = "itemName") String itemName) {
        var checkout = new CheckOut(getPricingRules(), checkoutRepository);
        try {
            checkout.scan(checkoutId, itemName);
            return ResponseEntity.ok("Scanned successfully.");
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Failed to scan. " + e.getMessage());
        }
    }

    @RequestMapping(value = "/total-price", method = RequestMethod.GET)
    ResponseEntity<String> getTotalPrice(@RequestParam(value = "checkoutId") Long checkoutId) {
        var checkout = new CheckOut(getPricingRules(), checkoutRepository);
        try {
            return ResponseEntity.ok(checkout.total(checkoutId).toString());
        } catch (Exception e) {
            return ResponseEntity.badRequest().body("Cannot get the total price. " + e.getMessage());
        }
    }

    // Get pricingRules from database for each API request to make sure the pricingRules always up-to-date
    private Map<String, Item> getPricingRules() {
        Map<String, Item> pricingRules = new HashMap<>();
        var items = itemRepository.findAll();
        items.forEach(item -> pricingRules.put(item.name, item));

        return pricingRules;
    }
}
