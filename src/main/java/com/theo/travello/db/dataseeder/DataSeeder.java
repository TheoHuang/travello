package com.theo.travello.db.dataseeder;

import com.theo.travello.db.entity.Item;
import com.theo.travello.db.repository.ItemRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

@Component
public class DataSeeder implements ApplicationRunner {

    @Autowired
    private ItemRepository itemRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        var items = new ArrayList<Item>();

        var itemA = new Item();
        itemA.name = "A";
        itemA.unitPrice = 50D;
        itemA.hasSpecialDeal = true;
        itemA.specialPriceForUnits = 130D;
        itemA.unitNumberForSpecialPrice = 3L;
        items.add(itemA);

        var itemB = new Item();
        itemB.name = "B";
        itemB.unitPrice = 30D;
        itemB.hasSpecialDeal = true;
        itemB.specialPriceForUnits = 45D;
        itemB.unitNumberForSpecialPrice = 2L;
        items.add(itemB);

        var itemC = new Item();
        itemC.name = "C";
        itemC.unitPrice = 20D;
        itemC.hasSpecialDeal = false;
        items.add(itemC);

        var itemD = new Item();
        itemD.name = "D";
        itemD.unitPrice = 15D;
        itemD.hasSpecialDeal = false;
        items.add(itemD);

        itemRepository.saveAll(items);
    }
}
