package com.theo.travello.db.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.HashMap;
import java.util.Map;

@Entity
@Table(name = "checkout")
public class Checkout {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "checkout_id")
    public Long checkoutId;

    @ElementCollection
    @CollectionTable(name = "scanned_items_mapping", joinColumns = {@JoinColumn(name = "checkout_id", referencedColumnName = "checkout_id")})
    @MapKeyColumn(name = "item_name")
    @Column(name = "numberOfUnits")
    public Map<String, Long> scannedItems = new HashMap<>();
}
