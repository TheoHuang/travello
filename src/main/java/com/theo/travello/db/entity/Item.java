package com.theo.travello.db.entity;

import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

@Entity
@Table(name = "item")
public class Item {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "native")
    @GenericGenerator(name = "native", strategy = "native")
    @Column(name = "item_id")
    public Long itemId;

    // It is individual letters of the alphabet (A, B, C, and so on) and case sensitive.
    @Column(name = "name", unique = true)
    public String name;

    @Column(name = "unit_price")
    public Double unitPrice;

    @Column(name = "has_special_deal")
    public Boolean hasSpecialDeal = false;

    @Column(name = "special_price_for_units")
    public Double specialPriceForUnits;

    @Column(name = "unit_number_for_special_price")
    public Long unitNumberForSpecialPrice;
}
