package com.theo.travello.model;

import com.theo.travello.db.entity.Item;
import com.theo.travello.db.repository.CheckoutRepository;

import java.util.Map;
import java.util.Set;

public class CheckOut {

    private Map<String, Item> pricingRules;

    private Set<String> scannableItems;

    private CheckoutRepository checkoutRepository;

    // Generate a new Checkout instance
    public CheckOut(Map<String, Item> pricingRules, CheckoutRepository checkoutRepository) {
        this.pricingRules = pricingRules;
        this.scannableItems = this.pricingRules.keySet();
        this.checkoutRepository = checkoutRepository;
    }

    // Scan items and add items to the Checkout instance
    public Map<String, Long> scan(Long checkoutId, String itemName) throws Exception {
        if (!this.scannableItems.contains(itemName)) {
            throw new Exception("Cannot scan this item because the item name cannot be found in the pricing rules.");
        }

        var checkoutOptional = checkoutRepository.findById(checkoutId);
        checkoutOptional.orElseThrow(() -> new Exception("The checkout ID is invalid."));

        var checkout = checkoutOptional.get();
        checkout.scannedItems.put(itemName, checkout.scannedItems.getOrDefault(itemName, 0L) + 1);

        return checkoutRepository.save(checkout).scannedItems;
    }

    // Get total price of the Checkout instance
    public Double total(Long checkoutId) throws Exception {
        var checkoutOptional = checkoutRepository.findById(checkoutId);
        checkoutOptional.orElseThrow(() -> new Exception("The checkout ID is invalid."));

        var checkout = checkoutOptional.get();

        final var ref = new Object() {
            double totalPrice = 0;
        };

        checkout.scannedItems.forEach((itemName, itemNumber) -> {
            var itemInfo = this.pricingRules.get(itemName);
            if (itemInfo.hasSpecialDeal) {
                ref.totalPrice += itemNumber % itemInfo.unitNumberForSpecialPrice * itemInfo.unitPrice + Math.floorDiv(itemNumber, itemInfo.unitNumberForSpecialPrice) * itemInfo.specialPriceForUnits;
            } else {
                ref.totalPrice += itemNumber * itemInfo.unitPrice;
            }
        });

        return ref.totalPrice;
    }
}
