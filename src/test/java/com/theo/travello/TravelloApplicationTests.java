package com.theo.travello;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@AutoConfigureMockMvc
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
class TravelloApplicationTests {

	@Autowired
	protected MockMvc mockMvc;

	protected <Request, Response> void callApi(Request request,
											   Response expect,
											   MockHttpServletRequestBuilder requestBuilder,
											   ResultMatcher httpStatus) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		var response = mockMvc.perform(requestBuilder
						.contentType(MediaType.ALL)
						.content(mapper.writeValueAsString(request))
						.accept(MediaType.ALL))
						.andExpect(httpStatus)
						.andReturn()
						.getResponse()
						.getContentAsString();
		Assertions.assertEquals(expect, response);
	}

	@Test
	void testStartCheckoutAPI() throws Exception {
		callApi(null, "1", get("/start-checkout"), MockMvcResultMatchers.status().isOk());
		callApi(null, "2", get("/start-checkout"), MockMvcResultMatchers.status().isOk());
		callApi(null, "3", get("/start-checkout"), MockMvcResultMatchers.status().isOk());
	}

	@Test
	void testScanAPI() throws Exception {
		callApi(null, "1", get("/start-checkout"), MockMvcResultMatchers.status().isOk());

		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());

		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=B"), MockMvcResultMatchers.status().isOk());

		callApi(null, "Failed to scan. Cannot scan this item because the item name cannot be found in the pricing rules.", get("/scan?checkoutId=1&itemName=Z"), MockMvcResultMatchers.status().isBadRequest());

		callApi(null, "Failed to scan. The checkout ID is invalid.", get("/scan?checkoutId=100&itemName=A"), MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	void testTotalPriceAPI4A1B() throws Exception {
		callApi(null, "1", get("/start-checkout"), MockMvcResultMatchers.status().isOk());

		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=B"), MockMvcResultMatchers.status().isOk());

		// Since 1 A => 50, 3 A => 130, 1 B => 30, so the result should be 50 + 130 + 30 = 210
		callApi(null, "210.0", get("/total-price?checkoutId=1"), MockMvcResultMatchers.status().isOk());
	}

	@Test
	void testTotalPriceAPI2A2B1C1D() throws Exception {
		callApi(null, "1", get("/start-checkout"), MockMvcResultMatchers.status().isOk());

		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=A"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=B"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=B"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=C"), MockMvcResultMatchers.status().isOk());
		callApi(null, "Scanned successfully.", get("/scan?checkoutId=1&itemName=D"), MockMvcResultMatchers.status().isOk());

		// Since 1 A => 50, 3 A => 130, 1 B => 30, 2 B => 45, 1 C => 20, 1 D => 15, so the result should be 50 * 2 + 45 + 20 + 15 = 180
		callApi(null, "180.0", get("/total-price?checkoutId=1"), MockMvcResultMatchers.status().isOk());
	}
}
